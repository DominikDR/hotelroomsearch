import React from 'react';
import PropTypes from 'prop-types';
import './Room.scss';

const Room = ({
    room : {
        image,
        name,
        roomType,
        totalPrice,
        bedroomsCount,
        doubleBedsCount,
        singleBedsCount
    }
}) => {
    return (
        <div className="room-container">
            <img className="room-img-preview" src={image} alt="hotel room preview" />
            <div className="room-description">
                <div className="room-title">
                    <div className="room-name ellipsis">{name}</div>
                    <span className="room-type">({roomType})</span>
                </div>
                <div className="room-details">
                    <span>Sypialnie: {bedroomsCount}</span>
                    {doubleBedsCount > 0 && <span>Łóżka podwójne: {doubleBedsCount}</span>}
                    {singleBedsCount > 0 && <span>Łóżka pojedyncze: {singleBedsCount}</span>}
                </div>
                <div className="room-price-box">
                    <span className="room-price">Suma: {totalPrice} PLN</span>
                </div>
            </div>
        </div>
    )
}

export { Room }; 

Room.propTypes = {
    room: PropTypes.shape({
        image: PropTypes.string,
        name: PropTypes.string,
        roomType: PropTypes.string,
        totalPrice: PropTypes.number,
        bedroomsCount: PropTypes.number,
        doubleBedsCount: PropTypes.number,
        singleBedsCount: PropTypes.number,
    }),
};
