import { useState } from 'react';
import jsonp from 'jsonp';

export const useFetchData = () => {
    const [data, setData] = useState();
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);

    const submitFetch = (url) => {
        jsonp(url, null, (err, data) => {
            setIsLoading(true);
            setIsError(false);
            if (err) {
                console.error(err.message);
                setIsError(true);
                setIsLoading(false);
            } else {
                setData(data);
                setIsLoading(false);
            }
        });
    }

    return [{ data, isLoading, isError }, submitFetch];
};
