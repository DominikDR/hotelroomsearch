import React from 'react';
import { useFetchData } from './useFetchData';
import { RoomList } from '../RoomList';
import { Form } from '../Form';
import { Loader } from '../Loader';
import './App.scss';

const App = () => {
    const [{ data, isLoading, isError }, doFetch] = useFetchData();

    const fetchFormData = (formData) => {
        const { adults, children, dateFrom, dateTo } = formData;
        doFetch(
            `http://testapi.itur.pl/api.php?date_from=${dateFrom}&date_to=${dateTo}&nb_adults=${adults}&nb_children=${children}&callback=callbackFunction`
        )
    };

    return (
        <div className="App">
            <header className="App-header">
                <Form handleSubmitForm={fetchFormData} />
            </header>
            {isError && <div>Nie udało się pobrać danych. Spróbuj ponownie</div>}
            {isLoading ? <Loader /> : <RoomList rooms={data} />}
        </div>
    );
}

export { App };
