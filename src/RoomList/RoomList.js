import React from 'react';
import PropTypes from 'prop-types';
import { Room } from '../Room';
import './RoomList.scss';

const RoomList = ({ rooms }) => {
    if (!rooms) {
        return null
    };
    return (
        <section className="room-list">
            {rooms.length === 0 && <div className="not-found-case">Nie znaleziono ofert dla wprowadzonych danych</div>}
            {rooms.map(room => <Room key={room.id} room={room} />)}
        </section>
    )
}

export { RoomList };

Room.propTypes = {
    rooms: PropTypes.arrayOf(PropTypes.object),
};
