import React, { useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { MDBBtn } from 'mdbreact';
import { DateRangePicker } from 'react-dates';
import { Input } from './Input';
import './Form.scss';

const Form = ({ handleSubmitForm }) => {
    const [guestCount, setGuestCount] = useState({
        adults: 0,
        children: 0,
    });
    const [focusedInput, setFocusedInput] = useState(null)
    const [date, setDate] = useState({})

    const handleInput = (e) => {
        setGuestCount({...guestCount,
            [e.target.name]: e.target.value,
        })
    }
    const parseDate = (dateToParse, dateFormatInString) => {
        return moment(dateToParse).format(dateFormatInString);
    }
    const onSubmit = (e) => {
        e.preventDefault();
        const parsedStartDate = parseDate(date.startDate, 'YYYY-MM-DD');
        const parsedEndDate = parseDate(date.endDate, 'YYYY-MM-DD');
        handleSubmitForm({...guestCount, 
            dateFrom: parsedStartDate,
            dateTo: parsedEndDate,
        });
    }

    return (
        <div>
            <form className="main-seach-form" onSubmit={onSubmit}>
                <span className="form-info-title">Proszę wybrać liczbę gości i datę pobytu aby zobaczyć ceny</span>
                <div className="guest-input-box form-inline">
                    <Input
                        label="Liczba dorosłych"
                        name="adults"
                        minValue={1}
                        onInput={handleInput}
                        required={true}
                    />
                    <Input
                        label="Liczba dzieci"
                        name="children"
                        minValue={0}
                        onInput={handleInput}
                        required={false}
                    />
                </div>
                <DateRangePicker
                    startDate={date.startDate}
                    startDateId="dateFrom"
                    startDatePlaceholderText="Od kiedy"
                    endDate={date.endDate}
                    endDateId="dateTo"
                    endDatePlaceholderText="Do kiedy"
                    onDatesChange={setDate}
                    focusedInput={focusedInput}
                    onFocusChange={setFocusedInput}
                    showClearDates
                    showDefaultInputIcon
                    inputIconPosition="before"
                    phrases={{
                        calendarLabel: 'Kalendarz',
                        closeDatePicker: 'Zamknij',
                        clearDates: 'Wyczyść',
                    }}
                    displayFormat="DD-MM-YYYY"
                    readOnly
                    required={true}
                />
                <MDBBtn type="submit">Szukaj</MDBBtn>
            </form>
        </div>
    );
}

export { Form };

Form.propTypes = {
    handleSubmitForm: PropTypes.func.isRequired,
};
