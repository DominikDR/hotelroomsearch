import React from "react";
import PropTypes from 'prop-types';
import { MDBInput } from "mdbreact";

const Input = ({ label, name, minValue, onInput, required }) => (
    <MDBInput
        className="mdb-input"
        label={label}
        type="number"
        min={minValue}
        validate
        success="right"
        name={name}
        outline
        size="lg"
        onInput={onInput}
        required={required}
    />
);

export { Input };

Input.propTypes = {
    label: PropTypes.string,
    name: PropTypes.string,
    minValue: PropTypes.number,
    onInput: PropTypes.func.isRequired,
    required: PropTypes.bool,
};
