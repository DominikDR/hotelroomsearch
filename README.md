## About the Hotel Room Search
This is widget which allows to user search for available accommodation in places such as hotels. Results are dependent from guest number and range of days. Project uses Material Design concepts and React (Hooks).

### Live version
You can see live version [Hotel Rooms Search Live](https://dominikdr.gitlab.io/hotelroomsearch/)
Note that after clicking search button your browser may block the content.
Then you need to `Unblock and allow insecure content` but don't worry.
This is caused by using third-party API in this APP to show examplary results.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Available Scripts

In the project directory, you can run:

#### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
